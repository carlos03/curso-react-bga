import * as Types from "./../constant/dataTypes";

const defaultState = {
  listaCasa: [],
  defaultListaCasa: [],
};

export default function dataReducer(state = defaultState, action) {
  if (action.type === Types.ADD_DATA) {
    return {
      ...state,
      listaCasa: action.listaCasa,
      defaultListaCasa: action.listaCasa,
    };
  } else {
    return state;
  }
}
