import { combineReducers } from "redux";
import dataReducer from "./dataReducer";
import searchBoxReducer from "./searchBoxReducer";

const rootReducer = combineReducers({
  searchBox: searchBoxReducer,
  casas: dataReducer,
});

export default rootReducer;
