
import * as Types from "./../constant/searchBoxTypes";

const defaultState = {
  name: "",
};

export default function searchBoxReducer(state = defaultState, action) {
  if (action.type === Types.SEARCH_BY_NAME) {
    return {
      ...state,
      name: action.name,
    };
  } else {
    return state;
  }
}
