import * as Types from "./../constant/dataTypes";

export const addDataAction = (data) => {
  return {
    type: Types.ADD_DATA,
    listaCasa: data,
  };
};
