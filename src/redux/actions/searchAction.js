import { addDataAction } from "./dataAction";

export const serachByName = (textSearch) => {
  return (dispatch, getState) => {
    const listaCasa = getState().casas.defaultListaCasa;
    const result = !textSearch
      ? listaCasa
      : listaCasa.filter((o) => {
          return Object.keys(o).some((k) => {
            if (typeof o[k] === "string")
              return o[k].toLowerCase().includes(textSearch.toLowerCase());
          });
        });
    dispatch(addDataAction(result));
  };
};
