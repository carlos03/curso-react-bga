import React, { useEffect } from 'react'
import {useSelector} from 'react-redux'
import Propiedad from './Propiedad'
import '../styles/listaPropiedades.css'

const ListaPropiedades = props => {

    const listaCasa = useSelector(state => state.casas.listaCasa);
    useEffect(() => {
      }, [listaCasa]);
    return (
        <div className="lista-propiedades">
            {
                listaCasa.length===0?
                ("sin resultados..."):
                (listaCasa.map((element,key) => {
                    return(
                        <Propiedad data={element} key={key} />
                    )
                }))
            }

            
        </div>
    )
}


export default ListaPropiedades
