import { useState } from 'react';
import '../../styles/header.css';
import {useDispatch} from 'react-redux';
import { serachByName } from '../../redux/actions/searchAction';

const SearchBox = ()=>{

  const dispatch = useDispatch();
  const [filter, onChangeTextInput] = useState('');
  const _handlerTextSearch=(text)=>{
    onChangeTextInput(text);
    dispatch(serachByName(text));
  }
  return(
    <div className="search-box">
        <input
          type="search"
          placeholder="ingrese texto"
          value={filter}
          onChange={event => _handlerTextSearch(event.currentTarget.value)}
        />
    </div>
  )
}
export default SearchBox;