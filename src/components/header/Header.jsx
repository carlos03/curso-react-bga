import '../../styles/header.css';
import Logo from './Logo';
import SearchBox from './SearchBox';

const Header = ()=>{

  return(
    <div className="container-header">
        <Logo />
        <div><h4>Filtrar por cualquier palabra</h4></div>
        <SearchBox/>
    </div>
  )
}
export default Header;