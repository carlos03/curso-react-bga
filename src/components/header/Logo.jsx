import '../../styles/header.css';
import logo from "./../../assets/logo-banco-ganadero-verde.png";
const Logo = ()=>{

  return(
    <div className="container-logo">
        <img src={logo} alt="logo" className="logo-header" height='auto' width='true'/>
        <span>Logo</span>
    </div>
  )
}
export default Logo;