import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux'
import Home from '../components/Home'
import '../styles/main.css'
import { lista } from '../assets/lista'
import { addDataAction } from '../redux/actions/dataAction';

const Propiedades = () => {
    const dispatch = useDispatch();
    useEffect(()=>{
        dispatch(addDataAction(lista));
    },[dispatch]);
    return (
        <div className="contenedor">
            <Home title={"Casas en Bolivia"} data={lista} />
        </div>
    )
}

export default Propiedades
